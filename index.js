

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const orderRoutes= require("./routes/order");
const productRoutes= require("./routes/product");
const userRoutes= require("./routes/user");
const cartRoutes= require("./routes/cart");

const app = express();


// connect to MONGODB:-----------------
mongoose.connect("mongodb+srv://dbcristyvilla:D4pe7npdr9J6r7ct@wdc028-course-booking.ktimy.mongodb.net/cp2-ecommerce-copy?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);
mongoose.connection.once("open", ()=> console.log("Now connected to MongoDB Atlas"));
// -------------------------------------


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/orders", orderRoutes);
app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/cart", cartRoutes);

app.listen(process.env.PORT || 4001, ()=> {
	console.log(`API is now online on port ${process.env.PORT || 4001 }`)
});


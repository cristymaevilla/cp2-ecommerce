const mongoose = require("mongoose");

const cartSchema= new mongoose.Schema({
	userId:{
		type: String,
		required:[true, "User Id is required"]
	},

	addedOn: {
		type: Date,
		default: new Date()
	},
	productId: {
		type: String,
		required:[true, "Product Id is required"]
	},
	name:{
		type:String,
		required:[true, "Name is required"]
	},
	quantity:{
		type:Number,
		min:1,
		required:[true,  "Quantity is required"]
	},
	stocks:{
		type:Number,
		required:[true, "Number of Stocks is required"]
	},
	price:{
		type:Number,
		required:[true, "Price is required"]
	},		
	subTotal: {
		type:Number,
		required : [true, "Subtotal is required"]
	},
	isActive:{
		type:Boolean,
		default: true
	}

});


module.exports = mongoose.model("Cart", cartSchema);
